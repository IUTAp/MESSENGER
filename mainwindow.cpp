#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //get daynamic memory
    cansel=new QPushButton;
    sign_in=new QPushButton;
    sign_up=new QPushButton;
    buttons=new QHBoxLayout;
    mainlayout=new QVBoxLayout;
    message=new QLabel;

    //central widget
    QWidget *central=new QWidget(this);
    central->setLayout(mainlayout);
    setCentralWidget(central);

    //set text
    message->setText(tr("WELCOME TO IUTGRAM"));
    sign_in->setText(tr("SIGN IN"));
    sign_up->setText(tr("SIGN UP"));
    cansel->setText(tr("Exit"));

    //set stylesheets
    setStyleSheet("background-color:#FFCCCC;\nborder:1px solid #990033;\nborder-radius:10px;");
    message->setStyleSheet("color:#990033;\nfont:40px;\nborder:none");
    message->setAlignment(Qt::AlignHCenter);
    sign_in->setStyleSheet("color:#990033;\nfont:15px;\nborder:2px solid #990033;\nborder-radius:10px;\npadding : 3px");
    sign_up->setStyleSheet("color:#990033;\nfont:15px;\nborder:2px solid #990033;\nborder-radius:10px;\npadding : 3px");
    cansel->setStyleSheet("color:#990033;\nfont:15px;\nborder:2px solid #990033;\nborder-radius:10px;\npadding : 3px");

    //design layouts
    buttons->addWidget(sign_up);
    buttons->addWidget(sign_in);
    mainlayout->addWidget(message);
    mainlayout->addLayout(buttons);
    mainlayout->addWidget(cansel);

    //set main window size
    setFixedSize(470,150);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    //connects
    connect(cansel,SIGNAL(clicked(bool)),this,SLOT(close()));

    //cursor
    sign_in->setCursor(Qt::PointingHandCursor);
    sign_up->setCursor(Qt::PointingHandCursor);
    cansel->setCursor(Qt::PointingHandCursor);

}

MainWindow::~MainWindow()
{
    delete ui;
}
