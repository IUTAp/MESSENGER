#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QPushButton>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QPushButton *cansel;
    QPushButton *sign_in;
    QPushButton *sign_up;
    QLabel *message;
    QVBoxLayout *mainlayout;
    QHBoxLayout *buttons;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
